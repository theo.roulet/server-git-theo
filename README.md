1. Déposer un manifest.json sur le git 

2. Il peut être consulté comme un fichier "brut" (raw) à l'adresse : 

`https://gitlab.com/theo.roulet/server-git-theo/-/raw/main/<nom_du_fichier_manifest.json>`

Par exemple : https://gitlab.com/theo.roulet/server-git-theo/-/raw/main/B372612102_Res_3702_V2-2_manifest.json

* Pour qu'il soit servi par gitlab - y compris à un client extérieur - il faut qu'il respecte le mécanisme [CORS - Cross Origin Resource Sharing](https://fr.wikipedia.org/wiki/Cross-origin_resource_sharing). 
  * *Il peut être accessible sans CORS, mais dans ce cas les échanges via http ne précisent pas la nature du fichier, et donc ne permettent pas aux clients de l'exploiter. Firefox, par exemple, ne comprend pas qu'il s'agit de JSON, et ne propose pas d'affichage adapté.*

3. Pour permettre le CORS, il faut utiliser https://raw.githack.com/ : un service de redirection / transformation de l'URL (ajoutant un header, donc un en tête à la réponse http, qui spécifie la nature du fichier).
  * Coller l'URL de git `-/raw/` dans le champ `paste an url here` 
  * Copier l'URL du champ de droite `Use this URL for development` 
  * Editer la propriété `@id` du manifest pour qu'elle corresponde à son url redigirigée par githack : 

```JSON
{
    "@context": "http://iiif.io/api/presentation/3/context.json",
    "id": "https://gl.githack.com/theo.roulet/server-git-theo/-/raw/main/B372612102_Res_3702_V2-2_manifest.json",
    
    "type": "Manifest",
    ... 
```

**Le service githack met 2 à 3 minutes pour donner accès à la page la première fois, tout comme pour répercuter chaque modification, c'est normal.**

4. L'`@id` du manifest doit désormais permettre de donner accès au fichier JSON bien interprété par firefox, comme pour [B372612102_Res_3702_V2-2_manifest.json](https://gl.githack.com/theo.roulet/server-git-theo/-/raw/main/B372612102_Res_3702_V2-2_manifest.json)

5. Le manifest, dûment servi, peut donc être testé sur https://iiif.io/api/presentation/validator/service/ , en collant cette URL `gl.githack` 
